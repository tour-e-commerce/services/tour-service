import { Test } from '@nestjs/testing';
import { INestApplication, ValidationPipe } from '@nestjs/common';
import { AppModule } from './../src/app.module';
import { PrismaService } from 'src/prisma/prisma.service';
import * as pactum from 'pactum';
import { CreateUserDto, roleDto } from 'src/common/dto';

describe('App e2e', () => {
  let app: INestApplication;
  let prisma: PrismaService;

  beforeAll(async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleRef.createNestApplication();

    app.useGlobalPipes(
      new ValidationPipe({
        whitelist: true,
      }),
    );
    await app.init();
    await app.listen(3333);

    prisma = app.get(PrismaService);
    await prisma.cleanDb();
    pactum.request.setBaseUrl('http://localhost:3333');
  });

  afterAll(() => {
    app.close();
  });

  describe('roles', () => {
    const dto: roleDto = {
      roleName: 'user',
    };
    describe('createRole', () => {
      it('shoud create role', () => {
        return pactum.spec().post('/roles').withBody(dto).expectStatus(201);
      });
      it('should throw if empty roleName', () => {
        return pactum
          .spec()
          .post('/roles')
          .withBody({
            roleName: '',
          })
          .expectStatus(400);
      });
    });
  });

  describe('auth', () => {
    const dto: CreateUserDto = {
      email: 'tranvinh2499@gmail.com',
      password: '12345678',
    };

    describe('signup', () => {
      it('should signup', () => {
        return pactum
          .spec()
          .post('/auth/signup')
          .withBody(dto)
          .expectStatus(201)
          .stores('token', 'access_token');
      });

      it('should throw if empty email', () => {
        return pactum
          .spec()
          .post('/auth/signup')
          .withBody({ password: dto.password })
          .expectStatus(400);
      });

      it('should throw if password not at least 8 characters', () => {
        return pactum
          .spec()
          .post('/auth/signup')
          .withBody({
            email: dto.email,
            password: '1234567',
          })
          .expectStatus(400);
      });
    });
  });

  // describe('user', () => {
  //   describe('Get me', () => {
  //     it('should get current user', () => {
  //       return pactum
  //         .spec()
  //         .get('/users/me')
  //         .withHeaders({
  //           Authorization: 'Bearer $S{token}',
  //         })
  //         .expectStatus(200);
  //     });
  //   });
  // });
});
