export type Review = {
  id: string;
  rating: number;
  userId: string;
  tourId: string;
  content: string;
  reviewReplies?: Array<any>;
};
