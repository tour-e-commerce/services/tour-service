export enum RoleId {
  USER = 1,
  ADMIN = 2,
  PROVIDER = 3,
}
