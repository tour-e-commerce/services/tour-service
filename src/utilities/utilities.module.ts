import { Module } from '@nestjs/common';
import { S3Service } from './s3.service';
import { ExcelService } from './excel.service';
import { FirebaseService } from './firebase.service';

@Module({
  providers: [S3Service, ExcelService, FirebaseService],
  exports: [S3Service, ExcelService, FirebaseService],
})
export class UtilitiesModule {}
