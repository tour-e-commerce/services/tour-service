// excel.service.ts
import { Injectable, NotFoundException } from '@nestjs/common';
import * as ExcelJS from 'exceljs';

@Injectable()
export class ExcelService {
  async validateExcel(buffer: Buffer): Promise<void> {
    const workbook = new ExcelJS.Workbook();
    await workbook.xlsx.load(buffer);

    // Define your template structure here
    const expectedHeaders = ['id', 'name', 'dob'];

    const sheet = workbook.getWorksheet(1);

    const headerRow = sheet.getRow(1);
    const actualHeaders = headerRow.values as string[];

    // Filter out empty cells or extra columns
    const trimmedActualHeaders = actualHeaders.filter((header) => !!header);

    if (!this.headersMatch(expectedHeaders, trimmedActualHeaders)) {
      throw new NotFoundException(
        'Uploaded Excel structure does not match the template.',
      );
    }
  }

  async excelToJson(buffer: Buffer): Promise<any[]> {
    await this.validateExcel(buffer);

    const workbook = new ExcelJS.Workbook();
    await workbook.xlsx.load(buffer);

    const sheet = workbook.getWorksheet(1); // Assuming you're working with the first sheet

    const headerRow = sheet.getRow(1);
    const columnHeaders = headerRow.values as string[];

    const result: any[] = [];

    for (let rowNumber = 2; rowNumber <= sheet.rowCount; rowNumber++) {
      const rowData = {};

      columnHeaders.forEach((header, colNumber) => {
        const cell = sheet.getCell(rowNumber, colNumber);
        rowData[header] = cell.value;
      });

      result.push(rowData);
    }

    return result;
  }

  private headersMatch(expected: string[], actual: string[]): boolean {
    if (expected.length !== actual.length) {
      return false;
    }

    for (let i = 0; i < expected.length; i++) {
      if (expected[i] !== actual[i]) {
        return false;
      }
    }

    return true;
  }
}
