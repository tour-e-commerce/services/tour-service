import * as argon from 'argon2';

export async function hashData(data: string) {
  return await argon.hash(data);
}
