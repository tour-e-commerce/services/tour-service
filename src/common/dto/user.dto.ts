import {
  IsDate,
  IsEmail,
  IsEnum,
  IsInt,
  IsNotEmpty,
  IsOptional,
  IsString,
  MinLength,
} from 'class-validator';
import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
export class ChangeUserPasswordDTO {
  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  oldPassword: string;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  @MinLength(8)
  newPassword: string;
}

export enum GENDER {
  MALE = 'M',
  FEMALE = 'F',
  UNKNOW = 'O',
}

export class UpdateUserInputDto {
  @ApiPropertyOptional()
  @IsString()
  @IsOptional()
  full_name?: string;

  @ApiPropertyOptional()
  @IsString()
  @IsOptional()
  dob?: string;

  @ApiPropertyOptional()
  @IsString()
  @IsOptional()
  avatar_image_url?: string;

  @ApiPropertyOptional()
  @IsString()
  @IsOptional()
  banner_image_url?: string;

  @ApiPropertyOptional()
  @IsEnum(GENDER)
  @IsOptional()
  gender?: string;

  @ApiPropertyOptional()
  @IsEmail()
  @IsOptional()
  email?: string;
}

export class UserDto {
  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  email?: string;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  password?: string;

  @ApiPropertyOptional()
  @IsString()
  @IsOptional()
  phone_number?: string;

  @ApiPropertyOptional()
  @IsString()
  @IsOptional()
  full_name?: string;

  @ApiPropertyOptional()
  @IsOptional()
  dob?: Date;

  @ApiPropertyOptional()
  @IsString()
  @IsOptional()
  gender?: string;

  @ApiPropertyOptional()
  @IsString()
  @IsOptional()
  avatar_image_url?: string;

  @ApiPropertyOptional()
  @IsString()
  @IsOptional()
  banner_image_url?: string;

  @ApiPropertyOptional()
  @IsString()
  @IsOptional()
  status?: number;

  @ApiPropertyOptional()
  @IsString()
  @IsOptional()
  country_code?: string;

  @ApiPropertyOptional()
  @IsString()
  @IsOptional()
  roleId?: number;

  @ApiPropertyOptional()
  @IsString()
  @IsOptional()
  rtHash?: string;

  @IsDate()
  @IsOptional()
  rtExprire?: Date;

  @IsInt()
  @IsOptional()
  report_count?: number;
}

export enum UserOrderBy {
  EMAIL_ASC = 'email:asc',
  EMAIL_DESC = 'email:desc',
  FULL_NAME_ASC = 'full_name:asc',
  FULL_NAME_DESC = 'full_name:desc',
  // Add more enum values for other properties and sorting options as needed
}

export class GetAllUserDto {
  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  search?: string;

  @ApiPropertyOptional()
  @IsOptional()
  @IsInt()
  page?: string;

  @ApiPropertyOptional()
  @IsOptional()
  @IsInt()
  limit?: string;

  @ApiPropertyOptional()
  @IsOptional()
  @IsEnum(UserOrderBy)
  orderBy?: string;
}

export class PaginationDto {
  @ApiPropertyOptional()
  @IsInt()
  take?: number;

  @ApiPropertyOptional()
  @IsInt()
  skip?: number;

  @ApiPropertyOptional()
  @IsString()
  query?: string;

  @ApiPropertyOptional()
  @IsEnum(UserOrderBy)
  orderBy?: string;
}
