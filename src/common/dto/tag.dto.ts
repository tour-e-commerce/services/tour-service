import { IsNotEmpty, IsEmail, IsString, MinLength, IsNumber, IsArray, ValidateNested, IsUUID, IsOptional, IsInt, IsEnum, IsNumberString } from 'class-validator';
import { Type } from 'class-transformer';
import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { OptionalParseIntPipe } from '../pipe/OptionalParseIntPipe.pipe';

export class GetAllTagDTO {

    @ApiPropertyOptional()
    @IsOptional()
    @Type(() => Number)
    tag_type?: number;

    @ApiPropertyOptional()
    @IsOptional()
    @IsString()
    search?: string;
}
