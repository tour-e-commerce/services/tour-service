import { IsString, IsOptional, } from 'class-validator';
import { Type } from 'class-transformer';
import { ApiPropertyOptional } from '@nestjs/swagger';

export class GetAllVehicleDTO {
    @ApiPropertyOptional()
    @IsOptional()
    @IsString()
    search?: string;
}
