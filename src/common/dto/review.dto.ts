import { ApiProperty } from '@nestjs/swagger';
import { IsEnum, IsNotEmpty, IsString } from 'class-validator';

enum RatingEnum {
  ONE = 1,
  TWO = 2,
  THREE = 3,
  FOUR = 4,
  FIVE = 5,
}

export class reviewDTO {
  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  @ApiProperty()
  content: string;
  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  tourId: string;
  @ApiProperty()
  @IsEnum(RatingEnum, {
    message: 'Invalid rating value. Please provide a rating between 1 and 5.',
  })
  rating: RatingEnum;
}

export class ReplyReviewDTO {
  @IsString()
  @IsNotEmpty()
  @ApiProperty()
  content: string;
  @IsString()
  @IsNotEmpty()
  @ApiProperty()
  reviewId: string;
}
