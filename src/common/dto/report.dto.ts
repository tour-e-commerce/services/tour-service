import { ApiProperty } from '@nestjs/swagger';
import {
  IsInt,
  IsNotEmpty,
  IsNumber,
  IsOptional,
  IsString,
} from 'class-validator';

export class ReportTypeDTO {
  @IsNotEmpty()
  @IsString()
  type_name: string;
}
export class ReportDTO {
  @IsString()
  @IsNotEmpty()
  @ApiProperty()
  targetUserId: string;

  @ApiProperty()
  @IsInt()
  @IsNumber()
  @IsNotEmpty()
  typeId: number;

  @ApiProperty()
  @IsString()
  @IsOptional()
  description: string;

  @ApiProperty()
  @IsString()
  @IsOptional()
  reportedContentId: string;
}
