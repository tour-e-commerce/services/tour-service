import { IsString, IsNotEmpty } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
export class roleDto {
  @IsString()
  @IsNotEmpty()
  @ApiProperty()
  roleName: string;
}
