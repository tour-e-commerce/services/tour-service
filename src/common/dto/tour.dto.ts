import { IsNotEmpty, IsEmail, IsString, MinLength, IsNumber, IsArray, ValidateNested, IsUUID, IsOptional, IsInt, IsEnum, ArrayUnique, IsNumberString } from 'class-validator';
import { Type } from 'class-transformer';
import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';

class TourComponent {
    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    title: string;

    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    description: string;
}

export class CreateOneTourDTO {
    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    name: string;

    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    description: string;

    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    footnote: string;

    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    tour_images: string;

    @ApiProperty()
    @IsNumber()
    @IsNotEmpty()
    price: number;

    @ApiProperty()
    @IsNumber()
    duration_day: number;

    @ApiProperty()
    @IsNumber()
    duration_night: number;

    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    location: string;

    @ApiProperty({ type: Array(String) })
    @IsArray()
    @IsNotEmpty()
    id_tags: string[];

    @ApiProperty({ type: Array(TourComponent) })
    @IsArray()
    @ValidateNested({ each: true })
    @Type(() => TourComponent)
    TourComponent: TourComponent[];


}

export class SingleTourDTO {
    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    name: string;

    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    description: string;

    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    footnote: string;

    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    tour_images: string;

    @ApiProperty()
    @IsNumber()
    @IsNotEmpty()
    price: number;

    @ApiProperty()
    @IsNumber()
    duration_day: number;

    @ApiProperty()
    @IsNumber()
    duration_night: number;

    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    location: string;

    @ApiProperty({ type: Array(String) })
    @IsArray()
    @IsNotEmpty()
    id_tags: string[];

    @ApiProperty({ type: Array(TourComponent) })
    @IsArray()
    @ValidateNested({ each: true })
    @Type(() => TourComponent)
    TourComponent: TourComponent[];
}

export enum TourOrderBy {
    NAME_ASC = 'name:asc',
    NAME_DESC = 'name:desc',
    PRICE_ASC = 'price:asc',
    PRICE_DESC = 'price:desc',
    // Add more enum values for other properties and sorting options as needed
}

export class GetAllTourDTO {
    @ApiPropertyOptional()
    @IsOptional()
    @IsString()
    search?: string;

    @ApiPropertyOptional()
    @IsOptional()
    @IsInt()
    @Type(() => Number)
    page?: number;

    @ApiPropertyOptional()
    @IsOptional()
    @IsInt()
    @Type(() => Number)
    limit?: number;

    @ApiPropertyOptional()
    @IsOptional()
    @IsEnum(TourOrderBy)
    orderBy?: string;

    @ApiProperty({ type: Array(Number) })
    @IsOptional()
    @IsArray()
    @ArrayUnique()
    tag_ids?: number[];

    @ApiProperty({ type: Array(Number) })
    @IsOptional()
    @IsArray()
    @ArrayUnique()
    vehicle_ids?: number[];

    @ApiPropertyOptional()
    @IsOptional()
    @IsString()
    address_name?: string;

    @ApiPropertyOptional()
    @IsOptional()
    @IsString()
    address_city?: string;

    @ApiPropertyOptional()
    @IsOptional()
    @IsString()
    address_province?: string;

    @ApiPropertyOptional()
    @IsOptional()
    @IsString()
    address_country?: string;

    @ApiPropertyOptional()
    @IsOptional()
    @IsString()
    tour_location_type?: string;

    @ApiPropertyOptional()
    @IsOptional()
    @Type(() => Number)
    upper_price_filter?: number;

    @ApiPropertyOptional()
    @IsOptional()
    @Type(() => Number)
    lower_price_filter?: number;

}
