import { PipeTransform, Injectable, ArgumentMetadata, BadRequestException } from '@nestjs/common';

@Injectable()
export class SanitizeStringPipe implements PipeTransform<string, string> {
    transform(value: string, metadata: ArgumentMetadata): string {
        if (!value) {
            return ''; // Return an empty string if the value is falsy
        }

        // Remove non-alphanumeric characters and extra spaces from the string
        return value.replace(/[^a-zA-Z0-9\s]/g, '').replace(/\s+/g, ' ').trim();
    }
}
