// import { GetAllTagDTO } from './../dto/tag.dto';


import { Tag } from "@prisma/client";
import { GetAllTagDTO } from "../dto/tag.dto";



export interface TagDAO {
    getAllTag(GetAllTagDTO: GetAllTagDTO): Promise<Tag[]>
    getTagById(id: number): Promise<JSON>


}