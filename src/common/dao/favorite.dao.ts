export interface FavoriteDao {
  createFavorites(userId: string, tourId: string): Promise<any>;
  getFavoritesByUserId(userId: string): Promise<Array<any>>;
  deleteFavorites(tourId: string, userId: string): Promise<any>;
}
