
import { Vehicle } from "@prisma/client";
import { GetAllVehicleDTO } from "../dto/vehicle.dto"


export interface VehicleDAO {
    getAllVehicle(getAllVehicleDTO: GetAllVehicleDTO): Promise<Vehicle[]>
    getVehicleById(id: number): Promise<JSON>

}