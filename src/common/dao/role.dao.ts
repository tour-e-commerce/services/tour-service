import { Role } from '@prisma/client';
import { roleDto } from 'src/common/dto';

export interface RoleDao {
  createRole(dto: roleDto): Promise<Role>;
  getAllRoles(): Promise<Role[]>;
}
