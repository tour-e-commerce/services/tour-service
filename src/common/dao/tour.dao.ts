import { GetAllTourDTO } from '../dto/tour.dto';
import { Optional } from '@nestjs/common';
import { Tour } from '@prisma/client';
import { UUID } from 'crypto';

export interface TourDAO {
  getAllTour(GetAllTourDto): Promise<Tour[]>;
  getTourById(tourId: string): Promise<any>;
}
