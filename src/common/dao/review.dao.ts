import { TourReview } from '@prisma/client';
export interface ReviewDao {
  getReviewById(reviewId: string): Promise<TourReview>;
  createReview(
    tourId: string,
    userId: string,
    content: string,
    rating: number,
  ): Promise<TourReview>;
  getAllReviewsByTourId(tourId: string): Promise<any>;
  deleteReview(reviewId: string, userId: string): Promise<TourReview>;
}
