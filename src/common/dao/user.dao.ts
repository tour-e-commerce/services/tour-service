import { UserDto, GetAllUserDto } from 'src/common/dto';
import { User } from '@prisma/client';

export interface UserDao {
  getUniqueUser(dto: UserDto): Promise<User>;
  getUniqueUserById(userId: string): Promise<User>;
  getAllUsers(dto: GetAllUserDto): Promise<{
    total: number;
    Users: User[];
  }>;
  createUser(dto: UserDto): Promise<User>;
  updateUserById(userId: string, dto: UserDto): Promise<User>;
  updateUserPasswordByEmail(dto: UserDto): Promise<User>;
  updateUserProfile(userId: string, dto: UserDto): Promise<User>;
  updateUserPassword(userId: string, dto: UserDto): Promise<User>;
  deleteUserRtHash(userId: string): Promise<any>;
}
