export interface ReplyDAO {
  getReplyById(replyId: string): Promise<any>;
  createReply(reviewId: string, content: string, userId: string): Promise<any>;
  deleteReply(replyId: string): Promise<any>;
}
