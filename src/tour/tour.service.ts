import { Tour } from '@prisma/client';
import { TourAccessService } from './../data-access/tour-access.service';
import { Injectable } from '@nestjs/common';
import { GetAllTourDTO } from 'src/common/dto/tour.dto';

import { PrismaService } from 'src/prisma/prisma.service';
import { faker } from '@faker-js/faker';
import { ReviewDataAccess } from 'src/data-access/review-access.service';
import { ResponseDto } from 'src/common/dto/response.dto';
@Injectable()
export class TourService {
  constructor(
    private tourAccessService: TourAccessService,
    private readonly prisma: PrismaService,
    private readonly reviewAccess: ReviewDataAccess,
  ) { }

  async getAllTour(getAllTourDto: GetAllTourDTO): Promise<Tour[]> {
    try {
      // const { limit, page } = getAllTourDto;

      const tourData = await this.tourAccessService.getAllTour(getAllTourDto);

      return tourData;
    } catch (error) {
      // throw new error
      console.log(error);
    }
  }

  // async fakeCreateTour() {
  //   let i = 0;
  //   while (i < 30) {
  //     await this.prisma.tour.create({
  //       data: {
  //         name: faker.location.country(),
  //         description: faker.lorem.sentence(),
  //         duration_day: 1,
  //         duration_night: 1,
  //         footnote: faker.lorem.sentence(),
  //         tour_images: [faker.image.imageUrl()],
  //         location: faker.location.country(),
  //         price: faker.number.float(),
  //         provider_user_id: 'bbc56d15-ebdb-442d-b72a-ad790d3fc878',
  //       },
  //     });
  //     i++;
  //   }
  // }

  async getReviewsByTourId(tourId: string) {
    return this.reviewAccess.getAllReviewsByTourId(tourId);
  }

  async getTourDetail(tourId: string): Promise<ResponseDto<Tour>> {
    try {
      const tour = await this.tourAccessService.getTourById(tourId);

      const totalRatings = tour.tourReview.length;

      const sumRatings = tour.tourReview.reduce(
        (sum: number, rating) => sum + rating.rating,
        0,
      );
      const averageRating = totalRatings > 0 ? sumRatings / totalRatings : 0;

      return {
        ...tour,
        rating: averageRating,
      };
    } catch (error) {
      return error;
    }
  }
}
