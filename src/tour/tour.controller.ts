import { Controller, Get, HttpException, HttpStatus, Param, Post, Query } from '@nestjs/common';
import { TourService } from './tour.service';
import { GetAllTourDTO } from 'src/common/dto/tour.dto';
import { ApiParam, ApiQuery, ApiTags } from '@nestjs/swagger';
@ApiTags('tour')
@Controller('tour')
export class TourController {
  constructor(private readonly tourService: TourService) { }

  @Get()
  async getAllTour(@Query() getAllTourDto: GetAllTourDTO) {
    let { upper_price_filter, lower_price_filter } = getAllTourDto
    if (upper_price_filter && lower_price_filter || !(upper_price_filter && lower_price_filter)) {
      const tourData = await this.tourService.getAllTour(getAllTourDto);
      return {
        status: 200,
        message: 'Get All Tour Success',
        data: tourData,
      };

    } else {
      throw new HttpException({
        status: 400,
        message: `Please insert both "upper_price_filter" and "lower_price_filter" or neither`
      }, HttpStatus.BAD_REQUEST)
    }

  }

  @Get('/:id')
  @ApiParam({
    name: 'id',
    type: String,
  })
  async getTourDetail(@Param() param: { id: string }) {
    const tourDetail = await this.tourService.getTourDetail(param.id);
    return {
      status: 200,
      message: 'Get Tour Detail Success',
      data: tourDetail,
    };
  }
  @Get('review/:tourId')
  @ApiParam({
    name: 'tourId',
    type: String,
  })

  async getReviewByTourId(@Param() param: { tourId: string }) {
    return this.tourService.getReviewsByTourId(param.tourId);
  }

}
