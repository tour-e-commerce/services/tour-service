import { Module } from '@nestjs/common';
import { TourService } from './tour.service';
import { TourController } from './tour.controller';
import { TourAccessService } from 'src/data-access/tour-access.service';

import { DataAccessModule } from 'src/data-access/data-access.module';
import { UtilitiesModule } from 'src/utilities/utilities.module';

@Module({
  imports: [UtilitiesModule, DataAccessModule],
  controllers: [TourController],
  providers: [TourService, TourAccessService],
})
export class TourModule {}
