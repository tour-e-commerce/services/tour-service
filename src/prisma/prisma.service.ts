import { Injectable } from '@nestjs/common';
import { PrismaClient } from '@prisma/client';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class PrismaService extends PrismaClient {
  constructor(config: ConfigService) {
    super({
      datasources: {
        db: {
          url: config.get("PROD") ? config.get('DATABASE_URL') : config.get('DATABASE_LOCAL'),
        },
      },
    });
  }

  cleanDb() {
    return this.$transaction([this.role.deleteMany(), this.user.deleteMany()]);
  }
}
