import {
  Body,
  Controller,
  Get,
  Put,
  UseGuards,
  HttpCode,
  HttpStatus,
  Query,
  Post,
  UseInterceptors,
  UploadedFile,
  ParseFilePipe,
  MaxFileSizeValidator,
  FileTypeValidator,
  Delete,
  Param,
} from '@nestjs/common';
import { UsersService } from './users.service';
import { GetCurrentUser, Roles } from 'src/common/decorators';
import {
  ApiBearerAuth,
  ApiBody,
  ApiConsumes,
  ApiParam,
  ApiProperty,
  ApiQuery,
  ApiTags,
} from '@nestjs/swagger';
import { AtGuard, RolesGuard } from 'src/common/guards';
// import { ChangeUserPasswordDTO, UpdateUserInputDto } from 'src/common/dto';
import {
  ChangeUserPasswordDTO,
  GetAllUserDto,
  ReportDTO,
  UpdateUserInputDto,
  UserOrderBy,
} from 'src/common/dto';
import { FileInterceptor } from '@nestjs/platform-express';
import { ReplyReviewDTO, reviewDTO } from 'src/common/dto/review.dto';

enum Role {
  CUSTOMER = 1,
  ADMIN = 2,
}

const MAX_FILE_SIZE = 1024 * 1024 * 8;
const IMAGE_FILE_TYPE = /(jpg|jpeg|png|webp)$/;
class FileUploadDto {
  @ApiProperty({ type: 'string', format: 'binary' })
  file: any;
}
@Controller('users')
@ApiTags('users')
@UseGuards(AtGuard)
@ApiBearerAuth()
export class UsersController {
  constructor(private readonly usersService: UsersService) { }

  @Get('me')
  getProfile(@GetCurrentUser('id') userId: string) {
    return this.usersService.getProfile(userId);
  }

  @Put('me/avatar')
  @UseInterceptors(FileInterceptor('file'))
  @ApiConsumes('multipart/form-data')
  @ApiBody({
    description: 'file type jpg/jpeg/png',
    type: FileUploadDto,
  })
  @HttpCode(HttpStatus.OK)
  updateAvatar(
    @UploadedFile(
      new ParseFilePipe({
        validators: [
          new MaxFileSizeValidator({
            maxSize: MAX_FILE_SIZE,
          }),
          new FileTypeValidator({
            fileType: IMAGE_FILE_TYPE,
          }),
        ],
      }),
    )
    file: Express.Multer.File,
    @GetCurrentUser('id') userId: string,
  ) {
    return this.usersService.updateAvatar(file, userId);
  }
  @Put('me/banner')
  @UseInterceptors(FileInterceptor('file'))
  @HttpCode(HttpStatus.OK)
  @ApiConsumes('multipart/form-data')
  @ApiBody({
    description: 'file type jpg/jpeg/png',
    type: FileUploadDto,
  })
  updateBannerImage(
    @UploadedFile(
      new ParseFilePipe({
        validators: [
          new MaxFileSizeValidator({
            maxSize: MAX_FILE_SIZE,
          }),
          new FileTypeValidator({
            fileType: IMAGE_FILE_TYPE,
          }),
        ],
      }),
    )
    file: Express.Multer.File,
    @GetCurrentUser('id') userId: string,
  ) {
    return this.usersService.updateBannerImage(file, userId);
  }

  @HttpCode(HttpStatus.OK)
  @Put('me/update')
  updateProfile(
    @GetCurrentUser('id') userId: string,
    @Body() dto: UpdateUserInputDto,
  ) {
    return this.usersService.updateProfile(userId, dto);
  }

  @Put('me/update-password')
  @HttpCode(HttpStatus.OK)
  changePassword(
    @GetCurrentUser('id') userId: string,
    @Body() dto: ChangeUserPasswordDTO,
  ) {
    return this.usersService.changePassword(userId, dto);
  }

  @Get('mail')
  sendMail(@GetCurrentUser('email') email: string) {
    return this.usersService.sendMail(email);
  }

  @Get('')
  @Roles(Role.ADMIN)
  @UseGuards(RolesGuard)
  @ApiQuery({
    name: 'search',
    type: String,
    required: false,
  })
  @ApiQuery({
    name: 'limit',
    type: Number,
    required: false,
  })
  @ApiQuery({
    name: 'page',
    type: Number,
    required: false,
  })
  @ApiQuery({
    name: 'orderBy',
    enum: UserOrderBy,
    required: false,
  })
  getAllUsers(@Query() dto: GetAllUserDto) {
    return this.usersService.getAllUsers(dto);
  }

  @Post('favorites')
  @Roles(Role.CUSTOMER)
  @UseGuards(RolesGuard)
  createFavorites(
    @GetCurrentUser('id') userId: string,
    @Body()
    dto: {
      tourId: string;
    },
  ) {
    return this.usersService.addTourFavorites(userId, dto.tourId);
  }

  @HttpCode(HttpStatus.NO_CONTENT)
  @Delete('favorites/:id')
  @HttpCode(HttpStatus.NO_CONTENT)
  @ApiParam({
    name: 'id',
    required: true,
  })
  deleteFavorites(
    @Param() params: { id: string },
    @GetCurrentUser('id') userId: string,
  ) {
    return this.usersService.deleteTourFavorite(params.id, userId);
  }

  @Post('review')
  createReview(
    @GetCurrentUser('id') userId: string,
    @Body()
    dto: reviewDTO,
  ) {
    try {
      return this.usersService.createReview(dto, userId);
    } catch (error) {
      return error;
    }
  }

  @Post('review/reply')
  replyReview(
    @GetCurrentUser('id') userId: string,
    @Body()
    dto: ReplyReviewDTO,
  ) {
    return this.usersService.createReply(userId, dto.content, dto.reviewId);
  }

  @HttpCode(HttpStatus.NO_CONTENT)
  @Delete('review/:id')
  @ApiParam({
    name: 'id',
    required: true,
  })
  deleteReview(
    @GetCurrentUser('id') userId: string,
    @Param()
    param: {
      id: string;
    },
  ) {
    return this.usersService.deleteReview(userId, param.id);
  }

  @HttpCode(HttpStatus.NO_CONTENT)
  @Delete('review/:reviewId/reply/:replyId')
  @ApiParam({
    name: 'reviewId',
    required: true,
  })
  @ApiParam({
    name: 'replyId',
    required: true,
  })
  deleteReply(
    @GetCurrentUser('id') userId: string,
    @Param()
    param: {
      reviewId: string;
      replyId: string;
    },
  ) {
    return this.usersService.deleteReply(userId, param.replyId, param.reviewId);
  }

  // @Post('report')
  // createReportUser(
  //   @GetCurrentUser('id') userId: string,
  //   @Body() dto: ReportDTO,
  // ) {
  //   return this.usersService.createReport(userId, dto);
  // }
}
