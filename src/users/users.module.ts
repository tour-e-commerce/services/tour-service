import { Module } from '@nestjs/common';
import { UsersService } from './users.service';
import { UsersController } from './users.controller';
import { DataAccessModule } from 'src/data-access/data-access.module';
import { UtilitiesModule } from 'src/utilities/utilities.module';

@Module({
  controllers: [UsersController],
  providers: [UsersService],
  imports: [DataAccessModule, UtilitiesModule],
})
export class UsersModule {}
