import {
  Injectable,
  ForbiddenException,
  HttpStatus,
  InternalServerErrorException,
  HttpException,
  NotFoundException,
} from '@nestjs/common';
import { MailerService } from '@nestjs-modules/mailer';
import * as argon from 'argon2';
import {
  ChangeUserPasswordDTO,
  GetAllUserDto,
  ReportDTO,
  UpdateUserInputDto,
} from 'src/common/dto';
import { User } from '@prisma/client';
import { UserDataAcess } from 'src/data-access/user-access.service';
import { ResponseDto } from 'src/common/dto/response.dto';

import { FavoriteDataAccess } from 'src/data-access/favorite-access.service';
import { ReviewDataAccess } from 'src/data-access/review-access.service';
import { FirebaseService } from 'src/utilities/firebase.service';
import { ReplyAccessService } from 'src/data-access/reply-access.module';
import { reviewDTO } from 'src/common/dto/review.dto';
import { TourAccessService } from 'src/data-access/tour-access.service';
// import { ReportAccessService } from 'src/data-access/report-access.module';

@Injectable()
export class UsersService {
  constructor(
    private readonly mailerService: MailerService,
    private readonly userDataAccess: UserDataAcess,
    private readonly firebaseService: FirebaseService,
    private readonly favoriteAccess: FavoriteDataAccess,
    private readonly reviewAccess: ReviewDataAccess,
    private readonly replyAccess: ReplyAccessService,
    private readonly tourAccess: TourAccessService,
    // private readonly reportAccess: ReportAccessService,
  ) { }

  async getAllUsers(dto: GetAllUserDto): Promise<ResponseDto<User>> {
    try {
      const skip = dto.page
        ? (parseInt(dto.page) - 1) * (parseInt(dto.limit) || 10)
        : undefined;
      const take = parseInt(dto.limit) || 10;
      const query = dto.search || '';

      const users = await this.userDataAccess.getAllUsers({
        query: query,
        skip: skip,
        take: take,
        orderBy: dto.orderBy || null,
      });

      const usersPerPage = users.Users.length;
      const totalUsers = users.total;
      const currentPage = parseInt(dto.page) || 1; // Assuming dto.page contains the current page number

      const totalPages = Math.ceil(totalUsers / usersPerPage);

      return {
        status: HttpStatus.OK,
        message: 'get all users from database successfully',
        pagination: {
          total_count: totalUsers,
          per_page: usersPerPage,
          page: currentPage,
          page_count: totalPages,
        },
        data: users.Users,
      };
    } catch (error) {
      return {
        status: HttpStatus.INTERNAL_SERVER_ERROR,
        message: 'something when wrong when getting all users',
        error,
      };
    }
  }

  async getProfile(userId: string): Promise<ResponseDto<User>> {
    try {
      const currentUser = await this.userDataAccess.getUniqueUserById(userId);

      delete currentUser.password;
      delete currentUser.refresh_token;

      return {
        status: HttpStatus.OK,
        message: 'get current user successfully',
        data: currentUser,
      };
    } catch (error) {
      throw new InternalServerErrorException({
        status: HttpStatus.INTERNAL_SERVER_ERROR,
        message: 'something when wrong',
        error,
      });
    }
  }

  async updateProfile(
    userId: string,
    dto: UpdateUserInputDto,
  ): Promise<ResponseDto<User>> {
    try {
      const updateUser = await this.userDataAccess.updateUserProfile(userId, {
        full_name: dto.full_name,
        gender: dto.gender,
        dob: new Date(dto.dob),
        avatar_image_url: dto.avatar_image_url,
        banner_image_url: dto.banner_image_url,
        email: dto.email,
      });

      delete updateUser.password;
      delete updateUser.refresh_token;

      return {
        status: HttpStatus.ACCEPTED,
        message: 'update user successfully',
        data: updateUser,
      };
    } catch (error) {
      throw new InternalServerErrorException({
        status: true,
        message: 'update user successfully',
        error,
      });
    }
  }

  sendMail(email: string) {
    this.mailerService
      .sendMail({
        to: email, // list of receivers
        from: 'tranvinh2499@gmail.com', // sender address
        subject: 'Testing Nest MailerModule ✔', // Subject line
        text: 'welcome', // plaintext body
        html: '<b>welcome</b>', // HTML body content
      })
      .then(() => { })
      .catch(() => { });
  }

  async changePassword(
    userId: string,
    dto: ChangeUserPasswordDTO,
  ): Promise<ResponseDto<void>> {
    try {
      const user = await this.userDataAccess.getUniqueUserById(userId);

      const oldPwMatch = await argon.verify(user.password, dto.oldPassword);

      if (!oldPwMatch) throw new ForbiddenException('Incorrect password');

      const hash = await argon.hash(dto.newPassword);

      const updateUser = await this.userDataAccess.updateUserPassword(userId, {
        password: hash,
      });

      delete updateUser.password;
      delete updateUser.refresh_token;

      return {
        status: HttpStatus.ACCEPTED,
        message: 'change password success',
      };
    } catch (error) {
      throw new InternalServerErrorException({
        status: HttpStatus.INTERNAL_SERVER_ERROR,
        message: 'something went wrong when change password',
      });
    }
  }
  async updateBannerImage(
    file: Express.Multer.File,
    userId: string,
  ): Promise<ResponseDto<string>> {
    try {
      const url = await this.firebaseService.uploadFile('banner', file);

      await this.userDataAccess.updateUserProfile(userId, {
        banner_image_url: url,
      });

      return {
        status: HttpStatus.ACCEPTED,
        message: 'update banner success',
        data: url,
      };
    } catch (error) {
      throw new HttpException('error when updateAvatar', 400, {
        cause: error,
      });
    }
  }

  async updateAvatar(
    file: Express.Multer.File,
    userId: string,
  ): Promise<ResponseDto<string>> {
    try {
      const url = await this.firebaseService.uploadFile('avatar', file);

      await this.userDataAccess.updateUserProfile(userId, {
        avatar_image_url: url,
      });

      return {
        status: HttpStatus.ACCEPTED,
        message: 'update avatar success',
        data: url,
      };
    } catch (error) {
      throw new HttpException('error when updateAvatar', 400, {
        cause: error,
      });
    }
  }

  updatePhone() { }

  async addTourFavorites(userId: string, tourId: string) {
    try {
      const data = await this.favoriteAccess.createFavorites(userId, tourId);

      return data;
    } catch (error) {
      if (error.code === 'P2002')
        throw new HttpException(
          'tour is already in user favorites list',
          HttpStatus.BAD_REQUEST,
        );

      throw new HttpException(error, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  async deleteTourFavorite(userId: string, tourId: string) {
    try {
      const data = await this.favoriteAccess.deleteFavorites(tourId, userId);
      console.log(
        'file: users.service.ts:235 ~ deleteTourFavorite ~ data:',
        data,
      );
    } catch (error) {
      if (error.code === 'P2025')
        throw new HttpException(
          {
            status: HttpStatus.BAD_REQUEST,
            message: 'favoriteId not found',
          },
          400,
        );
      return error;
    }
  }

  async createReview(dto: reviewDTO, userId: string) {
    const tour = await this.tourAccess.getTourById(dto.tourId);

    const check: Array<any> = tour.tourReview;

    if (check.some((i) => i.user_id === userId))
      throw new HttpException(
        {
          message: 'User already review',
        },
        400,
      );

    try {
      const review = this.reviewAccess.createReview(
        dto.tourId,
        userId,
        dto.content,
        dto.rating,
      );
      return review;
    } catch (error) {
      return error;
    }
  }

  async deleteReview(userId: string, reviewId: string) {
    const check = await this.reviewAccess.getReviewById(reviewId);

    if (!check) throw new NotFoundException('review not found');

    if (check.user_id !== userId) {
      throw new HttpException(
        {
          message: 'dont have permission',
        },
        400,
      );
    }
    try {
      return await this.reviewAccess.deleteReview(reviewId);
    } catch (error) {
      return error;
    }
  }

  async createReply(userId: string, content: string, reviewId: string) {
    try {
      return await this.replyAccess.createReply(reviewId, content, userId);
    } catch (error) {
      return error;
    }
  }

  async deleteReply(userId: string, replyId: string, reviewId: string) {
    const review = await this.reviewAccess.getReviewById(reviewId);

    const replies = review.reviewReplies;

    if (!replies.some((o) => o.id === replyId))
      throw new NotFoundException('review not contain reply');

    const check = await this.replyAccess.getReplyById(replyId);

    if (!check) throw new NotFoundException('reply not found');

    if (check.user_id !== userId)
      throw new HttpException(
        {
          message: 'you dont have permission to delete',
        },
        400,
      );

    try {
      await this.replyAccess.deleteReply(replyId);
    } catch (error) {
      return error;
    }
  }

  // async createReport(
  //   userId: string,
  //   dto: ReportDTO,
  // ): Promise<ResponseDto<Report>> {
  //   const bool = await this.reportAccess.getReportByUserId(
  //     userId,
  //     dto.targetUserId,
  //   );
  //   if (bool) {
  //   }

  //   if (dto.reportedContentId) {
  //     // check if user have that content_id
  //     const check = await this.userDataAccess.getUserContent(dto.targetUserId);

  //     // handle if target user have tour review
  //     if (check.TourReview.some((o) => o.id === dto.reportedContentId)) {
  //       //
  //     }
  //     if (check.ReplyReview.some((o) => o.id === dto.reportedContentId)) {
  //       const report = await this.reportAccess.createReport(userId, dto);
  //       await this.replyAccess.updateReplyReportCount(dto.reportedContentId);
  //       return {
  //         status: HttpStatus.CREATED,
  //         message: 'create report successfully',
  //         data: report,
  //       };
  //     } else {
  //       throw new NotFoundException('user does not have this content');
  //     }
  //   }
  //   try {
  //     const report = await this.reportAccess.createReport(userId, dto);
  //     await this.userDataAccess.updateUserReportCount(dto.targetUserId);

  //     return {
  //       status: HttpStatus.CREATED,
  //       message: 'create report successfully',
  //       data: report,
  //     };
  //   } catch (error) {
  //     throw new HttpException(
  //       {
  //         status: 500,
  //         message: 'error when creating report',
  //         error: error,
  //       },
  //       500,
  //     );
  //   }
  // }
}
