import { Tag, Vehicle } from '@prisma/client';
import { GetAllTagDTO } from './../common/dto/tag.dto';
import { TagAccessService } from './../data-access/tag-access.service';
import { Injectable } from '@nestjs/common';
import { VehicleAccessService } from 'src/data-access/vehicle-access.service';
import { GetAllVehicleDTO } from 'src/common/dto/vehicle.dto';

@Injectable()
export class ResourceService {
    constructor(private tagAccessService: TagAccessService,
        private vehicleAccessService: VehicleAccessService

    ) {



    }
    async getAllTag(getAllTag: GetAllTagDTO): Promise<Tag[]> {

        return this.tagAccessService.getAllTag(getAllTag);
    }
    async getAllVehicle(getAllVehicleDTO: GetAllVehicleDTO): Promise<Vehicle[]> {
        return this.vehicleAccessService.getAllVehicle(getAllVehicleDTO);
    }
}
