import { Module } from '@nestjs/common';
import { ResourceService } from './resource.service';
import { ResourceController } from './resource.controller';
import { TagAccessService } from 'src/data-access/tag-access.service';
import { VehicleAccessService } from 'src/data-access/vehicle-access.service';

@Module({
  controllers: [ResourceController],
  providers: [ResourceService, TagAccessService, VehicleAccessService]
})
export class ResourceModule { }
