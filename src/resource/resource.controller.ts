import { GetAllTagDTO } from './../common/dto/tag.dto';
import { Controller, Get, Query } from '@nestjs/common';
import { ResourceService } from './resource.service';
import { ApiTags } from '@nestjs/swagger';
import { GetAllVehicleDTO } from 'src/common/dto/vehicle.dto';
@ApiTags('resource')
@Controller('resource')
export class ResourceController {
  constructor(private readonly resourceService: ResourceService) { }

  @Get('tag')
  async GetAllTag(@Query() getAllTagDTO: GetAllTagDTO) {
    const data = await this.resourceService.getAllTag(getAllTagDTO);
    return {
      status: 200,
      message: 'Get All Tag Success',
      data: data,
    };
  }

  @Get("vehicle")
  async GetAllVehicle(@Query() getAllVehicleDTO: GetAllVehicleDTO) {
    const data = await this.resourceService.getAllVehicle(getAllVehicleDTO)
    return {
      status: 200,
      message: "Get All Vehicle Success",
      data: data
    }
  }
}
