import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { AuthModule } from './auth/auth.module';
import { RolesModule } from './roles/roles.module';
import { UsersModule } from './users/users.module';
import { MailerModule } from '@nestjs-modules/mailer';
import { PrismaModule } from './prisma/prisma.module';
import { DataAccessModule } from './data-access/data-access.module';
import { TourModule } from './tour/tour.module';
import { ResourceModule } from './resource/resource.module';

import { UtilitiesModule } from './utilities/utilities.module';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
    }),
    PrismaModule,
    AuthModule,
    RolesModule,
    UsersModule,
    MailerModule.forRoot({
      transport: {
        host: process.env.HOST,
        auth: {
          user: process.env.USER,
          pass: process.env.PASS,
        },
      },
    }),
    DataAccessModule,
    TourModule,
    ResourceModule,

    UtilitiesModule,
  ],
})
export class AppModule {}
