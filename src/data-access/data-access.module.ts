import { Module } from '@nestjs/common';
import { UserDataAcess } from './user-access.service';
import { RoleAccessService } from './role-access.service';
import { TourAccessService } from './tour-access.service';
import { FavoriteDataAccess } from './favorite-access.service';
import { ReviewDataAccess } from './review-access.service';
import { TagAccessService } from './tag-access.service';
import { ReplyAccessService } from './reply-access.module';
// import { ReportAccessService } from './report-access.module';
import { VehicleAccessService } from './vehicle-access.service';
@Module({
  providers: [
    UserDataAcess,
    TourAccessService,
    RoleAccessService,
    FavoriteDataAccess,
    ReviewDataAccess,
    TagAccessService,
    ReplyAccessService,
    // ReportAccessService,
    VehicleAccessService
  ],
  exports: [
    UserDataAcess,
    TourAccessService,
    RoleAccessService,
    FavoriteDataAccess,
    ReviewDataAccess,
    TagAccessService,
    ReplyAccessService,
    // ReportAccessService,
    VehicleAccessService
  ],
})
export class DataAccessModule { }
