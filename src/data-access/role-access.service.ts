import { RoleDao } from 'src/common/dao/role.dao';
import { roleDto } from 'src/common/dto';
import { Injectable } from '@nestjs/common';
import { PrismaService } from 'src/prisma/prisma.service';
import { Role } from '@prisma/client';

@Injectable()
export class RoleAccessService implements RoleDao {
  constructor(private readonly prisma: PrismaService) {}

  async createRole(dto: roleDto): Promise<Role> {
    try {
      const newRole = await this.prisma.role.create({
        data: {
          role_name: dto.roleName,
        },
      });
      return newRole;
    } catch (error) {
      return error;
    }
  }
  async getAllRoles(): Promise<Role[]> {
    try {
      const roles = await this.prisma.role.findMany({});
      return roles;
    } catch (error) {
      return error;
    }
  }
}
