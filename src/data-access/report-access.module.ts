// import { Injectable } from '@nestjs/common';
// import { ReportDAO } from 'src/common/dao/report.dao';
// import { ReportDTO, ReportTypeDTO } from 'src/common/dto';
// import { PrismaService } from 'src/prisma/prisma.service';
// import { Report, ReportType } from '@prisma/client';

// @Injectable()
// export class ReportAccessService implements ReportDAO {
//   constructor(private readonly prisma: PrismaService) { }

//   async getReportByUserId(
//     userId: string,
//     targetReportId: string,
//   ): Promise<Report> {
//     return await this.prisma.report.findFirst({
//       where: {
//         reporter_user_id: userId,
//         targeted_user_id: targetReportId,
//       },
//     });
//   }

//   //admin role
//   async getReportByTargetUserId(userId: string): Promise<Report[]> {
//     return await this.prisma.report.findMany({
//       where: {
//         targeted_user_id: userId,
//       },
//     });
//   }

//   async createReportType(dto: ReportTypeDTO): Promise<ReportType> {
//     return await this.prisma.reportType.create({
//       data: {
//         type_name: dto.type_name,
//       },
//     });
//   }

//   async getAllReportType(): Promise<ReportType[]> {
//     return await this.prisma.reportType.findMany();
//   }

//   async createReport(userId: string, dto: ReportDTO): Promise<any> {
//     return await this.prisma.report.create({
//       data: {
//         reporter_user_id: userId,
//         type_id: dto.typeId,
//         targeted_user_id: dto.targetUserId,
//         description: dto?.description,
//         reported_content_id: dto?.reportedContentId,
//       },
//     });
//   }
// }
