import { Injectable } from '@nestjs/common';
import { Prisma, Tour } from '@prisma/client';
import { TourDAO } from 'src/common/dao/tour.dao';
import { GetAllTourDTO } from 'src/common/dto/tour.dto';
import { PrismaService } from 'src/prisma/prisma.service';
@Injectable()
export class TourAccessService implements TourDAO {
  constructor(private prisma: PrismaService) { }

  async getTourById(tourId: string): Promise<any> {
    try {
      const tour = await this.prisma.tour.findUnique({
        where: {
          id: tourId,

        },
        include: {
          TourReview: true,
          TourSchedule: true, _count: true, tag_id: true, vehicle_id: true, user_id: true,
          TourAvailability: true,
        },
      });
      return tour;
    }
    catch (error) {
      throw new Error("Internal Server Error")
    }
  }

  async getAllTour(getAllTourDto: GetAllTourDTO): Promise<Tour[]> {
    try {
      let { search, limit, page, orderBy } = getAllTourDto;
      let { address_name, address_city, address_province, address_country, tag_ids, vehicle_ids, tour_location_type } = getAllTourDto;
      let prismaOrderBy: Prisma.TourOrderByWithRelationInput | undefined =
        undefined;
      if (orderBy) {
        const [field, order] = orderBy.split(':');
        console.log(field, '-', order);
        prismaOrderBy = { [field]: order as 'asc' | 'desc' };
      }
      console.log("cai nay dang chay", address_province);
      // const where: Prisma.TourWhereInput = search
      //   ? {

      //     AND: [
      //       address_name && { address_name: { contains: address_name }},
      //       address_city && { address_city: { contains: address_city } },
      //       address_province && { address_province: { contains: address_province } },
      //       address_country && { address_country: { contains: address_country } },

      //     ],
      //     OR: [
      //       { name: { contains: search, mode: 'insensitive' } },
      //       { description: { contains: search, mode: 'insensitive' } },
      //     ],
      //   }
      //   : {};

      const skip = page ? (page - 1) * (limit || 10) : undefined;
      const take = limit || 10;
      const tourData = await this.prisma.tour.findMany({
        include: { TourSchedule: true, _count: true, tag_id: true, vehicle_id: true, user_id: true },
        skip: skip,
        take: take,
        where: {
          name: { contains: search, mode: 'insensitive' },
          address_name: { contains: address_name },
          address_city: { contains: address_city },
          address_province: { contains: address_province },
          address_country: { contains: address_country },
          // tag_id: {every: {id:}},
        },
        orderBy: prismaOrderBy,
      });
      return tourData;
    } catch (error) {
      console.log(error);
      throw new Error('Internal server error');
    }
  }
}
