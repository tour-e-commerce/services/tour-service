import { Injectable } from '@nestjs/common';
import { ReviewDao } from 'src/common/dao';
import { PrismaService } from 'src/prisma/prisma.service';
import { TourReview } from '@prisma/client';

@Injectable()
export class ReviewDataAccess implements ReviewDao {
  constructor(private readonly prisma: PrismaService) { }

  async getReviewById(reviewId: string): Promise<any> {
    return await this.prisma.tourReview.findUnique({
      where: {
        id: reviewId,
      },
      include: {
        ReviewReplies: true,
      },
    });
  }

  async deleteReview(reviewId: string): Promise<any> {
    return await this.prisma.$transaction([
      this.prisma.tourReview.delete({
        where: {
          id: reviewId,
        },
      }),
      this.prisma.reviewReply.deleteMany({
        where: {
          review_id: reviewId,
        },
      }),
    ]);
  }

  async createReview(
    tourId: string,
    userId: string,
    content: string,
    rating: number,
  ): Promise<TourReview> {
    const review = await this.prisma.tourReview.create({
      data: {
        tour_id: tourId,
        user_id: userId,
        content: content,
        rating: rating,
      },
    });
    return review;
  }

  async getAllReviewsByTourId(tourId: string): Promise<{
    total: number;
    data: TourReview[];
  }> {
    const reviews = await this.prisma.$transaction([
      this.prisma.tourReview.count({
        where: {
          tour_id: tourId,
        },
      }),
      this.prisma.tourReview.findMany({
        where: {
          tour_id: tourId,
        },
        include: {
          ReviewReplies: {
            include: {
              user: {
                select: {
                  avatar_image_url: true,
                  full_name: true,
                },
              },
            },
          },
        },
      }),
    ]);

    return {
      total: reviews[0],
      data: reviews[1],
    };
  }
}
