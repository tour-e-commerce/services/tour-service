import { Injectable } from '@nestjs/common';
import { Prisma, Tag, Tour, } from '@prisma/client';
import { TagDAO } from 'src/common/dao/tag.dao';
import { GetAllTagDTO } from 'src/common/dto/tag.dto';

import { PrismaService } from 'src/prisma/prisma.service';
import { stringify as uuidStringify } from 'uuid'
@Injectable()
export class TagAccessService implements TagDAO {
    constructor(private prisma: PrismaService) { }
    async getAllTag(getAllTagDto: GetAllTagDTO): Promise<Tag[]> {
        try {
            let dtoCheck = false
            if (getAllTagDto?.search || getAllTagDto?.tag_type) {
                dtoCheck = true
                const search = getAllTagDto?.search ? getAllTagDto.search : undefined;
                const tagType = getAllTagDto?.tag_type ? getAllTagDto.tag_type : undefined;
                const where: Prisma.TagWhereInput = search || tagType
                    ? {
                        AND: [
                            search ? { name: { contains: search, mode: 'insensitive' } } : {},

                        ]




                    }
                    : {};
                const tagData = await this.prisma.tag.findMany({
                    where: where
                })

                console.log(tagData);
                return tagData

            }

            // const splitOrderBy = orderBy.split(":");

            const tagData = await this.prisma.tag.findMany({
            })
            console.log(tagData);
            return tagData
        } catch (error) {
            console.log(error);
            throw new Error("Internal server error");
        }

    }
    getTagById(id: number): Promise<JSON> {
        throw new Error('Method not implemented.');
    }


}
