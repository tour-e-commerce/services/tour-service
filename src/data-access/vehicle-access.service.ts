
import { Injectable } from '@nestjs/common';
import { Prisma, Vehicle, } from '@prisma/client';
import { VehicleDAO } from 'src/common/dao/vehicle.dao';
import { GetAllVehicleDTO } from 'src/common/dto/vehicle.dto';
import { PrismaService } from 'src/prisma/prisma.service';
@Injectable()
export class VehicleAccessService implements VehicleDAO {
    constructor(private prisma: PrismaService) { }
    async getAllVehicle(getAllVehicleDto: GetAllVehicleDTO): Promise<Vehicle[]> {
        try {
            if (getAllVehicleDto?.search) {

                const search = getAllVehicleDto?.search ? getAllVehicleDto.search : undefined;
                const where: Prisma.VehicleWhereInput = search
                    ? {
                        AND: [
                            search ? { name: { contains: search, mode: 'insensitive' } } : {},
                        ]
                    }
                    : {};
                const vehicleData = await this.prisma.vehicle.findMany({
                    where: where
                })

                console.log(vehicleData);
                return vehicleData

            }
            // const splitOrderBy = orderBy.split(":");
            const vehicleData = await this.prisma.vehicle.findMany({
            })
            console.log(vehicleData);
            return vehicleData
        } catch (error) {
            console.log(error);
            throw new Error("Internal server error");
        }

    }
    getVehicleById(id: number): Promise<JSON> {
        throw new Error('Method not implemented.');
    }


}
