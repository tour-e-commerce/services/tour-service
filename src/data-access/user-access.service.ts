import { Injectable } from '@nestjs/common';
import { UserDao } from 'src/common/dao';
import { UserDto, PaginationDto } from 'src/common/dto';
import { Prisma } from '@prisma/client';
import { PrismaService } from 'src/prisma/prisma.service';
import { User } from '@prisma/client';
import { RoleId } from 'src/types/role';

enum UserStatus {
  //Active
  ACTIVE = 'A',
  //Inactive
  INACTIVE = 'I',
}

@Injectable()
export class UserDataAcess implements UserDao {
  constructor(private readonly prisma: PrismaService) { }

  async updateUserPassword(userId: string, dto: UserDto): Promise<User> {
    return await this.prisma.user.update({
      where: {
        id: userId,
      },
      data: {
        password: dto.password,
      },
    });
  }

  async updateUserProfile(userId: string, dto: UserDto): Promise<User> {
    return await this.prisma.user.update({
      where: {
        id: userId,
      },
      data: {
        full_name: dto?.full_name,
        gender: dto?.gender,
        avatar_image_url: dto?.avatar_image_url,
        banner_image_url: dto?.banner_image_url,
        dob: dto?.dob,
        email: dto?.email,
      },
    });
  }

  async getAllUsers(dto: PaginationDto): Promise<{
    total: number;
    Users: User[];
  }> {
    const { skip, take, orderBy, query } = dto;

    const where: Prisma.UserWhereInput = query
      ? {
        OR: [
          {
            email: {
              contains: query,
              mode: 'insensitive',
            },
          },
          {
            full_name: {
              contains: query,
              mode: 'insensitive',
            },
          },
        ],
      }
      : {};

    let prismaOrderBy: Prisma.TourOrderByWithRelationInput | undefined =
      undefined;

    if (orderBy) {
      const [field, order] = orderBy.split(':');
      prismaOrderBy = { [field]: order as 'asc' | 'desc' };
    }

    const users = await this.prisma.$transaction([
      this.prisma.user.count(),
      this.prisma.user.findMany({
        take: take,
        skip: skip,
        where: where,
        orderBy: prismaOrderBy,
      }),
    ]);

    return {
      total: users[0],
      Users: users[1],
    };
  }

  async getUniqueUserById(userId: string): Promise<User> {
    const user = await this.prisma.user.findUnique({
      where: {
        id: userId,
      },
    });
    return user;
  }

  async updateUserById(userId: string, dto: UserDto): Promise<User> {
    const user = await this.prisma.user.update({
      where: {
        id: userId,
      },
      data: {
        email: dto?.email,
        password: dto?.password,
        full_name: dto?.full_name,
        avatar_image_url: dto?.avatar_image_url,
        banner_image_url: dto?.banner_image_url,
        dob: dto?.dob,
        phone_number: dto?.phone_number,
        gender: dto?.gender,
        refresh_token: dto?.rtHash,
        refresh_token_expire: dto?.rtExprire,
      },
    });
    return user;
  }

  async getUniqueUser(dto: UserDto): Promise<User> {
    const user = await this.prisma.user.findFirst({
      where: {
        email: dto.email,
        role_id: dto?.roleId,
      },
    });

    return user;
  }

  async createUser(dto: UserDto): Promise<User> {
    return await this.prisma.user.create({
      data: {
        email: dto.email,
        password: dto.password,
        role_id: RoleId.USER,
        full_name: dto?.full_name,
        avatar_image_url: dto?.avatar_image_url,
        banner_image_url: dto?.banner_image_url,
        dob: dto?.dob,
        phone_number: dto?.phone_number,
        gender: dto?.gender,
        status: UserStatus.ACTIVE,
      },
    });
  }

  async deleteUserRtHash(userId: string): Promise<any> {
    const updateUser = await this.prisma.user.updateMany({
      where: {
        id: userId,
        refresh_token: {
          not: null,
        },
      },
      data: {
        refresh_token: null,
      },
    });
    return updateUser;
  }

  async updateUserPasswordByEmail(dto: UserDto): Promise<User> {
    const check = await this.prisma.user.findFirst({
      where: {
        email: dto.email,
        role_id: dto.roleId,
      },
    });

    return await this.prisma.user.update({
      where: {
        id: check.id,
      },
      data: {
        password: dto.password,
      },
    });
  }

  // async updateUserReportCount(userId: string): Promise<User> {
  //   return await this.prisma.user.update({
  //     where: {
  //       id: userId,
  //     },
  //     data: {
  //       received_report_count: {
  //         increment: 1,
  //       },
  //     },
  //   });
  // }

  // async resetUserReportCount(userId: string): Promise<User> {
  //   return await this.prisma.user.update({
  //     where: {
  //       id: userId,
  //     },
  //     data: {
  //       received_report_count: 0,
  //     },
  //   });
  // }

  async getUserContent(userId: string) {
    return await this.prisma.user.findUnique({
      where: {
        id: userId,
      },
      select: {
        TourReview: true,
        ReplyReview: true,
      },
    });
  }
}
