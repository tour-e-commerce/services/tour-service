import { Injectable } from '@nestjs/common';
import { ReplyDAO } from 'src/common/dao/reply.dao';
import { PrismaService } from 'src/prisma/prisma.service';

@Injectable()
export class ReplyAccessService implements ReplyDAO {
  constructor(private readonly prisma: PrismaService) { }
  async getReplyById(replyId: string): Promise<any> {
    return await this.prisma.reviewReply.findUnique({
      where: {
        id: replyId,
      },
    });
  }

  async createReply(
    reviewId: string,
    content: string,
    userId: string,
  ): Promise<any> {
    const reply = await this.prisma.reviewReply.create({
      data: {
        content: content,
        review_id: reviewId,
        user_id: userId,
      },
    });
    return reply;
  }

  deleteReply(replyId: string): Promise<any> {
    return this.prisma.reviewReply.delete({
      where: {
        id: replyId,
      },
    });
  }

  // async updateReplyReportCount(id: string) {
  //   return await this.prisma.reviewReply.update({
  //     where: {
  //       id: id,
  //     },
  //     data: {
  //       report_count: {
  //         increment: 1,
  //       },
  //     },
  //   });
  // }
  // async resetReplyReportCount(id: string) {
  //   return await this.prisma.reviewReply.update({
  //     where: {
  //       id: id,
  //     },
  //     data: {
  //       report_count: 0,
  //     },
  //   });
  // }
}
