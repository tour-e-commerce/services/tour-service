import { Injectable } from '@nestjs/common';
import { FavoriteDao } from 'src/common/dao';
import { PrismaService } from 'src/prisma/prisma.service';

@Injectable()
export class FavoriteDataAccess implements FavoriteDao {
  constructor(private readonly prisma: PrismaService) {}

  async deleteFavorites(tourId: string, userId: string): Promise<any> {
    const data = await this.prisma.favorite.delete({
      where: {
        user_id_tour_id: {
          user_id: userId,
          tour_id: tourId,
        },
      },
    });
    return data;
  }

  async createFavorites(userId: string, tourId: string): Promise<any> {
    const newFavorites = await this.prisma.favorite.create({
      data: {
        user_id: userId,
        tour_id: tourId,
      },
    });

    return newFavorites;
  }

  async getFavoritesByUserId(userId: string): Promise<any[]> {
    return await this.prisma.favorite.findMany({
      where: {
        user_id: userId,
      },
    });
  }
}
