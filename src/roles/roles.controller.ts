import { Controller, Get, Post, Body, UseGuards } from '@nestjs/common';
import { RolesService } from './roles.service';
import { ApiBearerAuth, ApiResponse, ApiTags } from '@nestjs/swagger';
import { roleDto } from 'src/common/dto';
import { AtGuard, RolesGuard } from 'src/common/guards';
import { Roles } from 'src/common/decorators';
@ApiTags('roles')
@Controller('roles')
@UseGuards(AtGuard, RolesGuard)
@Roles(2)
@ApiBearerAuth()
export class RolesController {
  constructor(private readonly rolesService: RolesService) {}

  @Post()
  @ApiResponse({
    status: 201,
    description: 'The record has been successfully created.',
  })
  @ApiResponse({ status: 403, description: 'Forbidden.' })
  createRole(@Body() dto: roleDto) {
    return this.rolesService.createRole(dto);
  }

  @Get()
  @ApiResponse({
    status: 200,
    description: 'Get records successfully',
  })
  getRole() {
    return this.rolesService.getRoles();
  }
}
