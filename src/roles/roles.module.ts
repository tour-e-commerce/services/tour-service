import { Module } from '@nestjs/common';
import { RolesService } from './roles.service';
import { RolesController } from './roles.controller';
import { DataAccessModule } from 'src/data-access/data-access.module';

@Module({
  controllers: [RolesController],
  providers: [RolesService],
  imports: [DataAccessModule],
})
export class RolesModule {}
