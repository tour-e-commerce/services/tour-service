import {
  HttpStatus,
  Injectable,
  InternalServerErrorException,
} from '@nestjs/common';
import { RoleAccessService } from 'src/data-access/role-access.service';
import { roleDto } from 'src/common/dto';
import { ResponseDto } from 'src/common/dto/response.dto';
import { Role } from '@prisma/client';

@Injectable()
export class RolesService {
  constructor(private readonly roleAccessService: RoleAccessService) {}

  async createRole(dto: roleDto): Promise<ResponseDto<Role>> {
    try {
      const newRole = await this.roleAccessService.createRole(dto);
      return {
        status: HttpStatus.CREATED,
        message: 'create new role success',
        data: newRole,
      };
    } catch (error) {
      throw new InternalServerErrorException({
        status: HttpStatus.INTERNAL_SERVER_ERROR,
        message: 'error when create new role',
        error: error,
      });
    }
  }

  async getRoles(): Promise<ResponseDto<Role[]>> {
    try {
      const roles = await this.roleAccessService.getAllRoles();
      return {
        status: HttpStatus.OK,
        message: 'get all roles success',
        data: roles,
      };
    } catch (error) {
      throw new InternalServerErrorException({
        status: HttpStatus.INTERNAL_SERVER_ERROR,
        message: 'error when get all roles',
        error: error,
      });
    }
  }
}
