import {
  Injectable,
  ForbiddenException,
  HttpStatus,
  BadRequestException,
  InternalServerErrorException,
  NotFoundException,
} from '@nestjs/common';
import * as argon from 'argon2';
import * as uuid from 'uuid';
import jwtDecode from 'jwt-decode';
import { JwtService } from '@nestjs/jwt';
import { Tokens } from 'src/auth/types';
import { MailerService } from '@nestjs-modules/mailer';
import {
  AuthDto,
  CreateUserDto,
  GoogleDTO,
  ResetPasswordDto,
} from 'src/common/dto';
import { UserDataAcess } from 'src/data-access/user-access.service';
import { User } from '@prisma/client';
import { ResponseDto } from 'src/common/dto/response.dto';
// import { faker } from '@faker-js/faker';
import { hashData } from 'src/common/utils';
import { RoleId } from 'src/types/role';
import { ConfigService } from '@nestjs/config';

type googleUser = {
  email: string;
  name: string;
  picture: string;
};

@Injectable()
export class AuthService {
  constructor(
    private readonly jwt: JwtService,
    private readonly mailerService: MailerService,
    private readonly userDataAccess: UserDataAcess,
    private readonly config: ConfigService,
  ) { }

  async signin(dto: AuthDto): Promise<ResponseDto<Tokens>> {
    try {
      const user = await this.userDataAccess.getUniqueUser({
        email: dto.email,
        roleId: RoleId.USER,
      });

      if (!user) throw new ForbiddenException('User not exist');

      const passwordMatches = await argon.verify(user.password, dto.password);

      if (!passwordMatches) throw new ForbiddenException('Password incorrect');

      const token = await this.getTokens(user);

      await this.updateRtHash(user.id, token.refresh_token);

      return {
        status: HttpStatus.OK,
        message: 'signin successfully',
        data: token,
      };
    } catch (error) {
      if (error instanceof ForbiddenException) {
        throw new BadRequestException({
          status: HttpStatus.BAD_REQUEST,
          message: 'Bad request: ' + error.message,
        });
      } else {
        throw new InternalServerErrorException({
          status: HttpStatus.INTERNAL_SERVER_ERROR,
          message: 'error when signin',
          error: error,
        });
      }
    }
  }

  async signup(dto: CreateUserDto): Promise<ResponseDto<Tokens>> {
    try {
      const hash = await hashData(dto.password);
      const newUser = await this.userDataAccess.createUser({
        email: dto.email,
        password: hash,
        avatar_image_url:
          'https://images.squarespace-cdn.com/content/v1/54b7b93ce4b0a3e130d5d232/1519987020970-8IQ7F6Z61LLBCX85A65S/icon.png?format=1000w',
      });

      const tokens = await this.getTokens(newUser);
      return {
        status: HttpStatus.CREATED,
        message: 'signup successfully',
        data: tokens,
      };
    } catch (error) {
      console.log(error);
      if (error.code === 'P2002') throw new ForbiddenException('user existed');
      throw new InternalServerErrorException({
        status: HttpStatus.INTERNAL_SERVER_ERROR,
        message: 'error when signup',
        error,
      });
    }
  }

  async googleLogin(dto: GoogleDTO): Promise<ResponseDto<Tokens>> {
    try {
      const decode: googleUser = jwtDecode(dto.accessToken);

      const user = await this.userDataAccess.getUniqueUser({
        email: decode.email,
        roleId: RoleId.USER,
      });

      if (!user) {
        const hash = await hashData('google-login');

        const newUser = await this.userDataAccess.createUser({
          email: decode.email,
          password: hash,
          avatar_image_url: decode.picture,
          full_name: decode.name,
        });

        const token = await this.getTokens(newUser);
        await this.updateRtHash(newUser.id, token.refresh_token);
        return {
          status: HttpStatus.OK,
          message: 'signup successfully',
          data: token,
        };
      }
      const token = await this.getTokens(user);
      await this.updateRtHash(user.id, token.refresh_token);
      return {
        status: HttpStatus.OK,
        message: 'signin successfully',
        data: token,
      };
    } catch (error) {
      throw new InternalServerErrorException({
        status: HttpStatus.INTERNAL_SERVER_ERROR,
        message: 'something happen',
        error,
      });
    }
  }

  async refresh(userId: string, rt: string): Promise<ResponseDto<Tokens>> {
    try {
      const user = await this.userDataAccess.getUniqueUserById(userId);

      if (!user) throw new ForbiddenException('Access denied');

      const rtMatches = argon.verify(user.refresh_token, rt);

      if (!rtMatches) throw new ForbiddenException('Access denied');

      const tokens = await this.getTokens(user);

      await this.updateRtHash(userId, tokens.refresh_token);

      return {
        status: HttpStatus.OK,
        message: 'refresh access token success',
        data: tokens,
      };
    } catch (error) {
      throw new InternalServerErrorException({
        status: HttpStatus.INTERNAL_SERVER_ERROR,
        message: 'error when refresh token',
        error: error.response,
      });
    }
  }

  async logout(userId: string) {
    try {
      await this.userDataAccess.deleteUserRtHash(userId);
    } catch (error) {
      throw new InternalServerErrorException({
        status: HttpStatus.INTERNAL_SERVER_ERROR,
        message: 'error when logout',
        error: error.response,
      });
    }
  }

  async forgotPassword(dto: ResetPasswordDto) {
    const user = await this.userDataAccess.getUniqueUser({
      email: dto.email,
    });

    if (!user) throw new NotFoundException('Email not exist');
    try {
      const newPassword = uuid.v4();
      // console.log('newPassword: ', newPassword);

      const hash = await argon.hash(newPassword);

      await this.userDataAccess.updateUserPasswordByEmail({
        email: dto.email,
        password: hash,
      });

      this.mailerService
        .sendMail({
          to: dto.email, // list of receivers
          from: 'tranvinh2499@gmail.com', // sender address
          subject: 'Reset password', // Subject line
          text: `your new password: ${newPassword}`, // plaintext body
          html: `<b>your new password: ${newPassword}</b>`, // HTML body content
        })
        .then(() => {
          return {
            status: HttpStatus.ACCEPTED,
            message:
              'New password have been sent to your email, please check your email',
          };
        })
        .catch((err) => {
          throw new InternalServerErrorException({
            status: HttpStatus.INTERNAL_SERVER_ERROR,
            message: 'error when sending mail',
            error: err,
          });
        });
    } catch (error) {
      throw new InternalServerErrorException({
        status: HttpStatus.INTERNAL_SERVER_ERROR,
        message: 'error when calling forget password method',
        error: error,
      });
    }
  }

  async getTokens(user: User) {
    const [at, rt] = await Promise.all([
      this.jwt.signAsync(
        {
          id: user.id,
          email: user.email,
          phone_number: user.phone_number,
          full_name: user.full_name,
          dob: user.dob,
          gender: user.gender,
          avatar_image_url: user.avatar_image_url,
          banner_image_url: user.banner_image_url,
          status: user.status,
          role_id: user.role_id,
        },
        {
          secret: this.config.get('JWT_AT_SECRET'),
          expiresIn: '30m',
        },
      ),
      this.jwt.signAsync(
        {
          id: user.id,
          email: user.email,
          phone_number: user.phone_number,
          full_name: user.full_name,
          dob: user.dob,
          gender: user.gender,
          avatar_image_url: user.banner_image_url,
          banner_image_url: user.banner_image_url,
          status: user.status,
          role_id: user.role_id,
        },
        {
          secret: this.config.get('JWT_RT_SECRET'),
          expiresIn: '30d',
        },
      ),
    ]);
    return {
      access_token: at,
      refresh_token: rt,
    };
  }

  async updateRtHash(userId: string, rt: string) {
    const hash = await hashData(rt);

    await this.userDataAccess.updateUserById(userId, {
      rtHash: hash,
    });

    // // Get the current date
    const currentDate = new Date();

    const futureDate = new Date(currentDate);

    futureDate.setDate(currentDate.getDate() + 30);

    await this.userDataAccess.updateUserById(userId, {
      rtExprire: futureDate,
    });
  }

  // async fetchFakeData() {
  //   for (let i = 0; i < 40; i++) {
  //     await this.userDataAccess.createUser({
  //       email: faker.internet.email(),
  //       password: faker.string.numeric(),
  //     });
  //   }
  // }
}
