import { Module, forwardRef } from '@nestjs/common';
import { AuthService } from './auth.service';
import { AuthController } from './auth.controller';
import { JwtModule } from '@nestjs/jwt/';
import { AtStrategy, RtStrategy } from 'src/auth/strategies';
import { PassportModule } from '@nestjs/passport';
import { UsersModule } from 'src/users/users.module';
import { DataAccessModule } from 'src/data-access/data-access.module';
import { UtilitiesModule } from 'src/utilities/utilities.module';

@Module({
  imports: [
    JwtModule.register({}),
    PassportModule,
    forwardRef(() => UsersModule),
    DataAccessModule,
    UtilitiesModule,
  ],
  controllers: [AuthController],
  providers: [AuthService, RtStrategy, AtStrategy],
})
export class AuthModule {}
