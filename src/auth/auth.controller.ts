import {
  Controller,
  Body,
  Post,
  HttpCode,
  HttpStatus,
  UseGuards,
} from '@nestjs/common';
import { AuthService } from './auth.service';
import { ApiBearerAuth, ApiResponse, ApiTags } from '@nestjs/swagger';
import { GetCurrentUser } from 'src/common/decorators';
import { AtGuard, RtGuard } from 'src/common/guards/';
import {
  AuthDto,
  CreateUserDto,
  GoogleDTO,
  ResetPasswordDto,
} from 'src/common/dto';

@ApiTags('auth')
@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @Post('signup')
  @ApiResponse({
    status: 201,
    description: 'The record has been successfully created.',
  })
  @ApiResponse({ status: 403, description: 'Forbidden.' })
  signup(@Body() dto: CreateUserDto) {
    return this.authService.signup(dto);
  }

  @Post('signin')
  @HttpCode(HttpStatus.OK)
  signin(@Body() dto: AuthDto) {
    return this.authService.signin(dto);
  }
  @ApiBearerAuth()
  @UseGuards(RtGuard)
  @Post('refresh')
  @HttpCode(HttpStatus.OK)
  refresh(
    @GetCurrentUser('id') userId: string,
    @GetCurrentUser('refreshToken') rt: string,
  ) {
    return this.authService.refresh(userId, rt);
  }

  @HttpCode(HttpStatus.OK)
  @Post('google-login')
  googleLogin(@Body() dto: GoogleDTO) {
    return this.authService.googleLogin(dto);
  }

  @UseGuards(AtGuard)
  @HttpCode(HttpStatus.NO_CONTENT)
  @Post('signout')
  @ApiBearerAuth()
  logout(@GetCurrentUser('id') userId: string) {
    return this.authService.logout(userId);
  }

  @Post('request-forget-password')
  forgotPassword(@Body() dto: ResetPasswordDto) {
    return this.authService.forgotPassword(dto);
  }

  // @Get('fake-data')
  // generateFakeData() {
  //   return this.authService.fetchFakeData();
  // }
}
